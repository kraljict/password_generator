# This application is a simple but very useful password generator. 

## This allows the user to randomly generate a secure password that will protect their accounts better from Brute-Force attacks by hackers.

<br/><br/>

# Here is the link to the application : https://secure-password-generator.netlify.app

<img src="password_generator_screenshot.png">